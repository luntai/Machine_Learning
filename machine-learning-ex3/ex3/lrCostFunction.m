function [J, grad] = lrCostFunction(theta, X, y, lambda)
%LRCOSTFUNCTION Compute cost and gradient for logistic regression with 
%regularization
%   J = LRCOSTFUNCTION(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples
n = size(X, 2); % number of features  Jinx
% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));
% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta
%
% Hint: The computation of the cost function and gradients can be
%       efficiently vectorized. For example, consider the computation
%
%           sigmoid(X * theta)
%
%       Each row of the resulting matrix will contain the value of the
%       prediction for that example. You can make use of this to vectorize
%       the cost function and gradient computations. 
%
% Hint: When computing the gradient of the regularized cost function, 
%       there're many possible vectorized solutions, but one solution
%       looks like:
%           grad = (unregularized gradient for logistic regression)
%           temp = theta; 
%           temp(1) = 0;   % because we don't add anything for j = 0  
%           grad = grad + YOUR_CODE_HERE (using the temp variable)
%
%one = ones(m,1);
%J_Mat = (-y').*log(sigmoid(theta'*X')) - (one' - y').*log(one' - sigmoid(theta'*X'));
%reg_Mat = (theta.^2);
%J += sum(J_Mat, 2)/m + sum(reg_Mat)*lambda/2/m;

%%%%%% Non-vectorized (loop) methods : Nick work! %%%%%
for i = 1: m
	J += -y(i,1) *log(sigmoid((theta')*X(i, :)')) - (1 - y(i, 1))*log(1 - sigmoid((theta')*X(i, :)'));
end;
J/=m;
reg = 0;
for i = 2: n
	reg += theta(i, 1)^2;
reg *= lambda/2/m;
J += reg;

%grad = X'*(sigmoid(X*theta) - y);
%grad =  grad./m;
%for i = 2: n
%	grad(i, 1) += lambda * theta(i, 1)/m;
%end;

grad = X'*(sigmoid(X*theta) - y)/m;
grad(2:end,:) += lambda * theta(2:end,:) / m;


% =============================================================
%J_Mat
%grad = grad(:);

end
