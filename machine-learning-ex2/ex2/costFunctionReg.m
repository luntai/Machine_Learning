function [J, grad] = costFunctionReg(theta, X, y, lambda)
%COSTFUNCTIONREG Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples
n = size(X, 2);
% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta


for i = 1: m
	J += -y(i,1) *log(sigmoid((theta')*X(i, :)')) - (1 - y(i, 1))*log(1 - sigmoid((theta')*X(i, :)'));
end;
J/=m;

reg = 0;
for i = 2: n
	reg += theta(i, 1)^2;
reg *= lambda/2/m;

J += reg;

%A vectorized implementation is:
grad = X'*(sigmoid(X*theta) - y);
grad =  grad./m;

for i = 2: n
	grad(i, 1) += lambda * theta(i, 1)/m;
end;

% =============================================================

end
